package com.formation.tp_hibernate;

import org.hibernate.Session;

import com.formation.tp_hibernate.configs.HibernateUtils;

public class Driver {

	public static void main(String[] args) {

		Session session = HibernateUtils.getSession();
		
		System.out.println("ok");
		
		session.close();
	}

}
