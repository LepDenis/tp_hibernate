package com.formation.tp_hibernate.repositories;

import com.formation.tp_hibernate.models.Adress;

public interface AdressRepository extends CrudRepository<Adress, Long>{

}
