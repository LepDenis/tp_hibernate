package com.formation.tp_hibernate.repositories;

import com.formation.tp_hibernate.models.Admin;

public interface AdminRepository extends CrudRepository<Admin, Long> {

}
