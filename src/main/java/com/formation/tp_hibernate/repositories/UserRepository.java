package com.formation.tp_hibernate.repositories;

import com.formation.tp_hibernate.models.User;

public interface UserRepository extends CrudRepository<User, String>{

}
