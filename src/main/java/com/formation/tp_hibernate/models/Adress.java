package com.formation.tp_hibernate.models;

import javax.persistence.*;

@Entity
public class Adress {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(length = 10, nullable = false)
	private Integer num;
	@Column(nullable = false)
	private String street;
	@Column(length = 20, nullable = false)
	@Enumerated(EnumType.STRING)
	private TypeAdress type;
	@Column
	private String complement;
	@Column(nullable = false)
	private String city;
	@Column(length = 10, nullable = false)
	private String zipCode;
	@Column(nullable = false)
	private String country;
	@Column(length = 10)
	private String name;
	@ManyToOne
	@JoinColumn(nullable = false)
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getComplement() {
		return complement;
	}

	public TypeAdress getType() {
		return type;
	}

	public void setType(TypeAdress type) {
		this.type = type;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
