package com.formation.tp_hibernate.services;

import java.util.Collection;
import java.util.Optional;

import com.formation.tp_hibernate.models.Adress;
import com.formation.tp_hibernate.repositories.AdressRepository;

public class AdressService implements AdressRepository{

	public Optional<Collection<Adress>> findAll() {
		return null;
	}

	public Optional<Adress> findById(Long id) {
		return null;
	}

	public int save(Adress m) {
		return 0;
	}

	public int delete(Adress m) {
		return 0;
	}

}
