package com.formation.tp_hibernate.services;

import java.util.Collection;
import java.util.Optional;

import com.formation.tp_hibernate.models.Admin;
import com.formation.tp_hibernate.repositories.AdminRepository;

public class AdminService implements AdminRepository {

	public Optional<Collection<Admin>> findAll() {
		return null;
	}

	public Optional<Admin> findById(Long id) {
		return null;
	}

	public int save(Admin m) {
		return 0;
	}

	public int delete(Admin m) {
		return 0;
	}

}
