package com.formation.tp_hibernate.services;

import java.util.Collection;
import java.util.Optional;

import com.formation.tp_hibernate.models.User;
import com.formation.tp_hibernate.repositories.UserRepository;

public class UserService implements UserRepository {

	public Optional<Collection<User>> findAll() {
		return null;
	}

	public Optional<User> findById(String id) {
		return null;
	}

	public int save(User m) {
		return 0;
	}

	public int delete(User m) {
		return 0;
	}

}
